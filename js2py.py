from ast import AST, Module
from typing import Dict, Any


def transform(js: str) -> AST:
    """Given a string of `js` code return a python `AST`.

    This is the function that needs to be implemented.
    """

    return Module(body=[])


def js2py(js: str, context: Dict[str, Any]) -> Dict[str, Any]:
    """Run `js` code (transformed to python) against `context` and get a new context"""

    ast = transform(js)
    code = compile(ast, filename='<ast>', mode='exec')
    exec(code, context)
    # __builtins__ is automatically added to the context on execution
    del context['__builtins__']
    return context
